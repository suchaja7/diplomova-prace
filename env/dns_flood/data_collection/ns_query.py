import argparse
import queue
import time
import re

import sqlite3
import threading
from typing import List

import dns.message
import dns.name
import dns.rdata
import dns.query

timeout_lock = threading.Lock()
timeout_cnt = 0

error_lock = threading.Lock()
error_cnt = 0
any_err_cnt = 0

result_lock = threading.Lock()
final_result = []


def get_zones(db_file: str):
    zns = {}

    try:
        conn = sqlite3.connect(db_file)

        cursor = conn.execute("SELECT id, name from t_zone")
        rows = cursor.fetchall()

        for row in rows:
            cursor = conn.execute('SELECT addr FROM t_nameserver WHERE zone_id = ?', [row[0]])
            srvs = cursor.fetchall()
            zns[row[1]] = srvs

    except Exception as e:
        print("Could not create database: %s" % e)

    return zns


def has_dnssec(result):
    if result and result.answer:
        for rr in result.answer:
            for record in rr:
                if record.rdtype == dns.rdatatype.RRSIG:
                    return True
    return False


def rfc_compliant(result):
    if result and result.answer:
        for rr in result.answer:
            for record in rr:
                if record.rdtype == dns.rdatatype.HINFO and "rfc8482" in str(record).lower():
                    return True
    return False


def gather_info(zone: str, servers: List[str]):
    results = []
    for ip in servers:
        addr = ip[0]
        try:
            any_result, any_q_size, any_r_size = query_server(zone, addr, dns.rdatatype.ANY)
            txt_result, txt_q_size, txt_r_size = query_server(zone, addr, dns.rdatatype.TXT)
            ns_result, ns_q_size, ns_r_size = query_server(zone, addr, dns.rdatatype.NS)
            dnssec_result, dnssec_q_size, dnssec_r_size = query_server(zone, addr, dns.rdatatype.RRSIG)

            # All queries failed. Nothing to analyze
            if any_result:
                payload = any_result.payload
            elif txt_result:
                payload = txt_result.payload
            elif ns_result:
                payload = ns_result.payload
            elif dnssec_result:
                payload = dnssec_result.payload
            else:
                continue

            rrsig = has_dnssec(dnssec_result)
            rfc = rfc_compliant(any_result)

            data = [zone, addr, any_q_size, any_r_size, txt_q_size, txt_r_size, ns_q_size, ns_r_size, dnssec_q_size, dnssec_r_size, rrsig, rfc, payload]
            tmp = ''
            for i in data:
                tmp = tmp + "," + str(i)
            results.append(tmp.strip(',') + '\n')
        except dns.exception.Timeout:
            global timeout_cnt
            timeout_lock.acquire()
            timeout_cnt = timeout_cnt + 1
            timeout_lock.release()
            pass
    return results


def query_server(zone: str, ip: str, qtype: dns.rdatatype):
    query = dns.message.make_query(zone, qtype, use_edns=True, payload=65535)
    try:
        response = dns.query.udp(query, ip, 3)
    except dns.exception.Timeout as te:
        raise te
    except Exception as e:
        global error_cnt, any_err_cnt
        error_lock.acquire()
        error_cnt = error_cnt + 1
        if qtype is dns.rdatatype.ANY:
            any_err_cnt = any_err_cnt + 1
        error_lock.release()
        return None, len(query.to_wire()), 0

    return response, len(query.to_wire()), len(response.to_wire())



def thread_worker(idx: int, q: queue.Queue, size: int):
    results = []
    now = int(time.time())
    while True:
        try:
            task = q.get(timeout=3)
        except queue.Empty:
            break

        results.append(gather_info(task[0], task[1]))
        q.task_done()
        elapsed = int(time.time())
        if elapsed - now >= 10:
            current_size = q.qsize()
            print("Thread %d -> Progress: %.1f%% (%d/%d)" % (
                idx, (1 - (current_size / size)) * 100, size - current_size, size)
                  )
            now = int(time.time())

    with result_lock:
        global final_result
        for result_set in results:
            final_result = final_result + result_set


def start_threads(q: queue.Queue, thread_num: int):
    size = q.qsize()
    threads = []
    for i in range(0, thread_num):
        thread = threading.Thread(target=thread_worker, args=(i, q, size))
        threads.append(thread)
        thread.start()

    for t in threads:
        t.join()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Program for enumerating DNS zones and authoritative nameservers')
    parser.add_argument('-o', type=str, required=True, help='output file')
    parser.add_argument('-t', type=int, default=1, choices=range(1, 6), help='number of threads')
    parser.add_argument('-d', type=str, required=True, help='DB file with zones info')
    args = parser.parse_args()

    task_queue = queue.Queue()
    zones = get_zones(args.d)

    for zone, servers in zones.items():
        task_queue.put_nowait([zone, servers])

    start_threads(task_queue, args.t)
    with open(args.o, 'w') as file:
        file.writelines(final_result)

    print("Timeout: %d\tFailed queries: %d" % (timeout_cnt, error_cnt))
