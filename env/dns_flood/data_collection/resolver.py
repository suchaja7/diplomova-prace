import argparse
import logging
import threading
import queue
import time

import dns.message
import dns.name
import dns.rdata
import dns.query
import sqlite3
import numpy

from typing import List

root_server = ['198.41.0.4',
               '199.9.14.201',
               '192.33.4.12',
               '199.7.91.13',
               '192.203.230.10',
               '192.5.5.241',
               '192.112.36.4',
               '198.97.190.53',
               '192.36.148.17',
               '192.58.128.30',
               '193.0.14.129',
               '199.7.83.42',
               '202.12.27.33']
timeout_lock = threading.Lock()
error_lock = threading.Lock()
cache_lock = threading.Lock()
result_lock = threading.Lock()

ns_cache = {}
ns_ips = {}
merged_results = {}
conn = None
timeout_count = 0
error_count = 0


def init_db(db_file: str):
    connection = sqlite3.connect(db_file)
    c = connection.cursor()

    c.execute('create table if not exists t_zone (id INTEGER PRIMARY KEY, name VARCHAR)')
    c.execute('create table if not exists t_nameserver (id INTEGER PRIMARY KEY, addr VARCHAR, zone_id INTEGER)')
    c.execute('create table if not exists t_zone_nameserver (id INTEGER PRIMARY KEY, zone_id INTEGER, ns_id INTEGER, FOREIGN KEY (zone_id) REFERENCES t_zone(id), FOREIGN KEY (ns_id) REFERENCES t_nameserver(id))')

    connection.commit()

    return connection


def store_results(domain: str, servers: List[str]):
    global conn
    cursor = conn.execute("SELECT COUNT(id) FROM t_zone WHERE name = ?", [domain])
    res = cursor.fetchone()[0]
    if res == 0:
        cursor = conn.execute("INSERT INTO t_zone (name) VALUES (?)", [domain])
        last_id = cursor.lastrowid
        for s in servers:
            if s not in ns_ips:
                try:
                    result, answer = resolve(s, qtype=dns.rdatatype.A)
                    ip = str(answer.answer[0][0])
                except Exception as ex:
                    logger.info("Could not resolve '%s': %s" % (domain, ex))
                    continue
            else:
                ip = ns_ips[s]
            cursor = conn.execute("INSERT INTO t_nameserver (addr, zone_id) VALUES (?, ?)", [ip, last_id])
        cursor.close()
        conn.commit()


def update_ns_list(authority: List[dns.rrset.RRset], additional: List[dns.rrset.RRset]):
    zone = authority[0].to_text().split(' ', 1)[0]
    zone = zone.rstrip('.')

    cache_lock.acquire()
    if zone in ns_cache:
        cache_lock.release()
        return

    servers = []
    for rr in additional:
        for record in rr:
            if record.rdtype == dns.rdatatype.A:
                servers.append(str(record))
                name = str(rr).split(' ')[0]
                if name not in ns_ips:
                    ns_ips[name] = str(record)
    if servers:
        ns_cache[zone] = servers
    cache_lock.release()


def find_ns(domain):
    cache_lock.acquire()
    domain = domain.rstrip('.')
    i = domain.find('.')
    while i != -1 and domain != '':
        i = domain.find('.')
        ns = ns_cache.get(domain, None)
        if ns:
            cache_lock.release()
            return ns
        domain = domain[i + 1:]
    cache_lock.release()
    return root_server


def resolve(domain, qtype, ips=None, loop_check=None):
    if qtype is dns.rdatatype.A:
        if loop_check is None:
            loop_check = [domain]
        else:
            if domain in loop_check:
                raise Exception("Loop detected")
            else:
                loop_check.append(domain)

    if not ips:
        ips = find_ns(domain)

    query = dns.message.make_query(domain, qtype)
    response = None
    for ip in ips:
        try:
            response = dns.query.udp(query, ip, 3)
            if response:
                break
        except dns.exception.Timeout:
            continue
    if not response:
        with timeout_lock:
            global timeout_count
            timeout_count += 1
            raise Exception("Timeout")

    resolved = False

    if response.rcode() == dns.rcode.SERVFAIL or response.rcode() == dns.rcode.NXDOMAIN:
        with error_lock:
            global error_count
            error_count += 1
            raise Exception("Received NXDOMAIN or SERVFAIL")

    if response.answer:
        resolved = True
        return resolved, response
    elif response.additional:
        if response.authority:
            update_ns_list(response.authority, response.additional)
        response, resolved = resolve(domain, qtype)

    elif response.authority and not resolved:
        nameserver = None
        for rr in response.authority:
            if rr.rdtype == dns.rdatatype.SOA and response.flags & dns.flags.AA:
                return True, response
            for record in rr:
                if record.rdtype == dns.rdatatype.NS:
                    result, ns_answer = resolve(str(record), dns.rdatatype.A, loop_check)
                    if result:
                        nameserver = str(ns_answer.answer[0][0])
                        with cache_lock:
                            if str(record) not in ns_ips:
                                ns_ips[str(record)] = nameserver
        response, resolved = resolve(domain, qtype, [nameserver])

    return response, resolved


def find_domain(domain: str, results: dict):
    try:
        result, record = resolve(domain, dns.rdatatype.NS)
        if result and record.answer:
            for rr in record.answer:
                if rr.rdtype == dns.rdatatype.NS:
                    ns_list = []
                    for r in rr:
                        ns_list.append(str(r))
                    results[domain] = ns_list
    except Exception as ex:
        logger.info("Could not resolve '%s': %s" % (domain, ex))


def thread_worker(idx: int, q: queue, size: int):
    results = {}
    now = int(time.time())
    while True:
        try:
            domain = q.get(timeout=3)
        except queue.Empty:
            with result_lock:
                merged_results.update(results)
            return

        find_domain(domain, results)
        elapsed = int(time.time())
        if elapsed - now >= 10:
            current_size = q.qsize()
            print("Thread %d -> Progress: %.1f%% (%d/%d)" % (
                idx, (1 - (current_size / size)) * 100,  size - current_size, size)
            )
            now = int(time.time())
        q.task_done()


def find_from_file(filename: str, thread_num: int):
    threads = []
    q = queue.Queue()

    with open(filename) as file:
        for line in file:
            q.put_nowait(line.strip())

    size = q.qsize()

    for i in range(0, thread_num):
        thread = threading.Thread(target=thread_worker, args=(i, q, size))
        threads.append(thread)
        thread.start()

    for i in range(0, len(threads)):
        threads[i].join()
        print("Thread %d finished" % i)

    print("Collecting results...")
    for domains, servers in merged_results.items():
        store_results(domains, servers)


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    parser = argparse.ArgumentParser(description='Program for enumerating DNS zones and authoritative nameservers')
    parser.add_argument('-f', type=str, help='file with domain names')
    parser.add_argument('-t', type=int, default=1, choices=range(1, 6), help='number of threads')
    parser.add_argument('-d', type=str, required=True, help='DB file for storing zones a nameservers')
    args = parser.parse_args()

    try:
        conn = init_db(args.d)
    except Exception as e:
        print("Could not create database: %s" % e)
        exit(1)

    try:
        find_from_file(args.f, args.t)
    finally:
        conn.close()

    print("Error count: %d\tTimeout count: %d\t" % (error_count, timeout_count))
