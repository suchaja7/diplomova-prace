#!/bin/bash
sudo mkdir /etc/bind/zones
cd /etc/bind/zones
sudo cp /home/vagrant/lab.com.zone .
sudo cp /home/vagrant/named.conf.local ../
sudo cp /home/vagrant/named.conf.options ../
sudo dnssec-keygen -a NSEC3RSASHA1 -b 2048 -n ZONE lab.com
sudo dnssec-keygen -f KSK -a NSEC3RSASHA1 -b 4096 -n ZONE lab.com
for key in `ls Klab.com*.key`; do 
	echo "\$INCLUDE $key" | sudo tee -a lab.com.zone
done
sudo dnssec-signzone -A -3 $(head -c 1000 /dev/urandom | sha1sum | cut -b 1-16) -N INCREMENT -o lab.com -t lab.com.zone
sudo service bind9 restart
cd /home/vagrant
mkdir config
mv -t config lab.com.zone named.conf.local named.conf.options