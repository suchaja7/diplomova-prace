#!/bin/bash
sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT
echo 1 | sudo tee /proc/sys/net/ipv4/ip_forward
echo 0 | sudo tee /proc/sys/net/ipv4/conf/all/rp_filter
echo 0 | sudo tee /proc/sys/net/ipv4/conf/enp0s8/rp_filter

echo "[Unit]
Description=Snort3 NIDS Daemon
After=syslog.target network.target
[Service]
Type=simple
ExecStart=/usr/local/bin/snort -c /usr/local/etc/snort/snort.lua -s 65535 \
-k none -l /var/log/snort -D -u snort -g snort -i enp0s9 -m 0x1b --create-pidfile \
--plugin-path=/usr/local/etc/so_rules/
[Install]
WantedBy=multi-user.target" | sudo tee /lib/systemd/system/snort3.service
sudo systemctl enable snort3
sudo service snort3 start
