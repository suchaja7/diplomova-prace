#!/bin/bash
sudo apt update
sudo apt install -y sysstat
sudo systemctl enable sysstat
echo 'ENABLED="true"' | sudo tee /etc/default/sysstat
sudo service sysstat restart